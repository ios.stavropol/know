//@ts-nocheck
import { createModel } from '@rematch/core';
import { API, StorageHelper } from './../../services';
import { ErrorsHelper } from './../../helpers';
import { Dispatch } from 'store';
import type { RootModel } from './../models';
import { BASE_URL } from './../../constants';
import Common from './../../utilities/Common';

type UserState = {
	isRequestGoing: false,
	userProfile: null,
	dictionaries: [],
	diskSpacePlans: [],
	userPlans: [],
	authorPlans: [],
};

const user = createModel<RootModel>()({
	state: {
		isRequestGoing: false,
		userProfile: null,
		dictionaries: [],
		diskSpacePlans: [],
		userPlans: [],
		authorPlans: [],
	} as UserState, 
	reducers: {
		setRequestGoingStatus: (state, payload: boolean) => ({
			...state,
			isRequestGoing: payload,
		}),
		setUserProfile: (state, payload: object) => ({
			...state,
			userProfile: payload,
		}),
		setDictionaries: (state, payload: object) => ({
			...state,
			dictionaries: payload,
		}),
		setDiskSpacePlans: (state, payload: object) => ({
			...state,
			diskSpacePlans: payload,
		}),
		setUserPlans: (state, payload: object) => ({
			...state,
			userPlans: payload,
		}),
		setAuthorPlans: (state, payload: object) => ({
			...state,
			authorPlans: payload,
		}),
	},
	effects: (dispatch) => {
		return {
			async getDictionaries() {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getDictionaries()
					.then(response => {
						console.warn('getDictionaries -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setDictionaries(response.data);
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.error);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async getDiskSpacePlans() {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getDiskSpacePlans()
					.then(response => {
						console.warn('getDiskSpacePlans -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setDiskSpacePlans(response.data);
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.error);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async getUserPlans() {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getUserPlans()
					.then(response => {
						console.warn('getUserPlans -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUserPlans(response.data);
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.error);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async getAuthorPlans() {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getAuthorPlans()
					.then(response => {
						console.warn('getAuthorPlans -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setAuthorPlans(response.data);
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.error);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async getCode(email) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.getCode(email)
					.then(response => {
						console.warn('getCode -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.error);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async login(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.login(payload.email, payload.code)
					.then(response => {
						console.warn('login -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							StorageHelper.saveData('token', response.data.token);
							API.setToken(response.data.token);
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						console.warn(err.response);
						if (err.response.data?.message?.length) {
							reject(err.response.data.message);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async mobileLogin(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.mobileLogin(payload)
					.then(response => {
						console.warn('mobileLogin -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							StorageHelper.saveData('token', response.data.token);
							API.setToken(response.data.token);
							resolve();
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						console.warn(err.response);
						if (err.response.data?.message?.length) {
							reject(err.response.data.message);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async getSelf() {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getSelf()
					.then(response => {
						console.warn('getSelf -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUserProfile(response.data);
							resolve(response.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						if (err.response.status == 400) {
							console.warn(err.response);
							reject(err.response.data.error);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async saveSelf(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.saveSelf(payload)
					.then(response => {
						console.warn('saveSelf -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUserProfile(response.data);
							resolve(response.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						console.warn(err.response);
						if (err.response.data?.message?.length) {
							reject(err.response.data.message);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async getSelfAuthor() {
				return new Promise((resolve, reject) => {
					// dispatch.user.setRequestGoingStatus(true);
					API.common.getSelfAuthor()
					.then(response => {
						console.warn('getSelfAuthor -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUserProfile(response.data);
							resolve(response.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						console.warn(err.response);
						if (err.response.status == 400) {
							reject(err.response.data.error);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async saveAuthor(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.saveAuthor(payload)
					.then(response => {
						console.warn('saveAuthor -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUserProfile(response.data);
							resolve(response.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						console.warn(err.response);
						if (err.response.data?.message?.length) {
							reject(err.response.data.message);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async createAuthor(payload) {
				return new Promise((resolve, reject) => {
					console.warn(payload);
					dispatch.user.setRequestGoingStatus(true);
					API.common.createAuthor(payload)
					.then(response => {
						console.warn('createAuthor -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 200) {
							dispatch.user.setUserProfile(response.data);
							resolve(response.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						console.warn(err.response);
						if (err.response.data?.message?.length) {
							reject(err.response.data.message);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
			async uploadPhoto(payload) {
				return new Promise((resolve, reject) => {
					dispatch.user.setRequestGoingStatus(true);
					API.common.uploadPhoto(payload)
					.then(response => {
						console.warn('uploadPhoto -> response', response);
						dispatch.user.setRequestGoingStatus(false);
						if (response.status == 201) {
							resolve(response.data);
						} else {
							reject(response.data.message);
						}
					})
					.catch(err => {
						dispatch.user.setRequestGoingStatus(false);
						console.warn(err.response);
						if (err.response.data?.message?.length) {
							reject(err.response.data.message);
						} else {
							reject('Произошла неизвестная ошибка! Повторите снова.');
						}
					});
				});
			},
		}
	},
});

export default user;