import { Models } from "@rematch/core";
import user from "./user";
import buttons from "./buttons";

export interface RootModel extends Models<RootModel> {
	user: typeof user;
	buttons: typeof buttons;
}

export const models: RootModel = { user, buttons };