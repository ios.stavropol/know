import React, { useEffect } from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import BlueButton from '../BlueButton';

const NewAuthor = ({navigation, action, style}) => {

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0),
			flex: 1,
			alignItems: 'center',
			justifyContent: 'space-between',
		}, style]}>
			<View>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
					fontFamily: 'Gilroy-ExtraBold',
					fontSize: Common.getLengthByIPhone7(36),
					color: colors.GRAY1_COLOR,
					marginTop: Common.getLengthByIPhone7(75),
				}}>
					Поздравляем Вы стали автором!
				</Text>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
					fontFamily: 'Gilroy-Bold',
					fontSize: Common.getLengthByIPhone7(24),
					color: colors.GRAY3_COLOR,
					marginTop: Common.getLengthByIPhone7(10),
				}}>
					Создай свой курс вместе с <Text style={{
					fontFamily: 'Gilroy-Bold',
					fontSize: Common.getLengthByIPhone7(24),
					color: colors.BLUE_COLOR,
					marginTop: Common.getLengthByIPhone7(10),
				}}>
					KNOW
				</Text>
				</Text>
			</View>
			<Image
				source={require('./../../assets/ic-create2.png')}
				style={{
					width: Common.getLengthByIPhone7(350),
					height: Common.getLengthByIPhone7(295),
					resizeMode: 'contain',
				}}
			/>
			<TouchableOpacity style={[{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(64),
				height: Common.getLengthByIPhone7(60),
				borderRadius: Common.getLengthByIPhone7(5),
				alignItems: 'center',
				justifyContent: 'space-between',
				backgroundColor: colors.BLUE_COLOR,
				marginBottom: Common.getLengthByIPhone7(54) + 86,
				flexDirection: 'row',
			}, style]}
			onPress={() => {
				navigation.navigate('ActivateAuthor', {action: action});
				// navigation.navigate('Rekvizits');
			}}>
				<Image
					source={require('./../../assets/ic-rocket.png')}
					style={{
						width: Common.getLengthByIPhone7(45),
						height: Common.getLengthByIPhone7(45),
						resizeMode: 'contain',
						marginLeft: Common.getLengthByIPhone7(10),
					}}
				/>
				<Text style={[{
					fontFamily: 'Gilroy-SemiBold',
					fontSize: Common.getLengthByIPhone7(18),
					color: colors.WHITE_COLOR,
				}]}
				allowFontScaling={false}>
					Создать новый курс
				</Text>
				<Image
					source={require('./../../assets/ic-arrow-right-white.png')}
					style={{
						width: Common.getLengthByIPhone7(5),
						height: Common.getLengthByIPhone7(10),
						resizeMode: 'contain',
						marginRight: Common.getLengthByIPhone7(19),
					}}
				/>
			</TouchableOpacity>
		</View>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(NewAuthor);