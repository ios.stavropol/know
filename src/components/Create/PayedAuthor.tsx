import React, { useEffect } from 'react';
import {TouchableOpacity, ScrollView, View, Image, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import BlueButton from '../BlueButton';

const PayedAuthor = ({navigation, action, style}) => {

	const renderView = (title, subtitle, active, style, onPress) => {
		return (<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(200),
			height: Common.getLengthByIPhone7(200),
			borderRadius: Common.getLengthByIPhone7(15),
			padding: Common.getLengthByIPhone7(18),
			backgroundColor: colors.BLUE_COLOR,
			opacity: active ? 1 : 0.5,
			justifyContent: 'flex-start',
		}, style]}
		activeOpacity={0.9}>
			<Text style={{
				fontFamily: 'Gilroy-Bold',
				fontSize: Common.getLengthByIPhone7(28),
				color: colors.WHITE_COLOR,
			}}>
				{title}
			</Text>
			<Text style={{
				fontFamily: 'Gilroy-Regular',
				fontSize: Common.getLengthByIPhone7(14),
				color: colors.WHITE_COLOR,
				marginTop: Common.getLengthByIPhone7(18),
			}}>
				{subtitle}
			</Text>
		</TouchableOpacity>);
	}

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0),
			flex: 1,
			alignItems: 'center',
			justifyContent: 'space-between',
		}, style]}>
			<View>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
					fontFamily: 'Gilroy-ExtraBold',
					fontSize: Common.getLengthByIPhone7(35),
					color: colors.GRAY1_COLOR,
					marginTop: Common.getLengthByIPhone7(75),
				}}>
					Создать новый курс
				</Text>
				<Image
					source={require('./../../assets/ic-create2.png')}
					style={{
						width: Common.getLengthByIPhone7(350),
						height: Common.getLengthByIPhone7(295),
						resizeMode: 'contain',
						marginTop: Common.getLengthByIPhone7(50),
					}}
				/>
			</View>
			<ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				height: Common.getLengthByIPhone7(200),
				marginBottom: Common.getLengthByIPhone7(40),
			}}
			horizontal={true}
			showsHorizontalScrollIndicator={false}
			contentContainerStyle={{
				alignItems: 'center',
				justifyContent: 'center',
			}}>
				{renderView('Бесплатный курс', 'Создайте бесплатный курс для начинающих. Помогите пользователям узнать о Вас и приобрести первые знания!', true, {
					marginLeft: Common.getLengthByIPhone7(15),
				}, () => {

				})}
				{renderView('Курс по подписке', 'Для создания платных курсов, необходимо разместить один бесплатный', false, {
					marginLeft: Common.getLengthByIPhone7(10),
				}, () => {
					
				})}
				{renderView('Платный курс', 'Для создания платных курсов, необходимо разместить один курс по подписке', false, {
					marginLeft: Common.getLengthByIPhone7(10),
					marginRight: Common.getLengthByIPhone7(15),
				}, () => {
					
				})}
			</ScrollView>
		</View>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(PayedAuthor);