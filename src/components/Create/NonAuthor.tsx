import React, { useEffect } from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import BlueButton from '../BlueButton';

const NonAuthor = ({navigation, refresh, style}) => {

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0),
			flex: 1,
			alignItems: 'center',
			justifyContent: 'space-between',
		}, style]}>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
				fontFamily: 'Gilroy-ExtraBold',
				fontSize: Common.getLengthByIPhone7(36),
				color: colors.GRAY1_COLOR,
				marginTop: Common.getLengthByIPhone7(75),
			}}>
				Заполни анкету и стань автором курсов
			</Text>
			<Image
				source={require('./../../assets/ic-create1.png')}
				style={{
					width: Common.getLengthByIPhone7(350),
					height: Common.getLengthByIPhone7(295),
					resizeMode: 'contain',
				}}
			/>
			<BlueButton
				style={{
					marginBottom: Common.getLengthByIPhone7(54) + 86,
				}}
				title={'Заполнить анкету'}
				onPress={() => {
					navigation.navigate('AuthorAnketa', {action: () => {
						if (refresh) {
							refresh();
						}
					}});
				}}
			/>
		</View>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(NonAuthor);