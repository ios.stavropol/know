import React, { useEffect } from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import BlueButton from '../BlueButton';

const ModerationView = ({navigation, refresh, style}) => {

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0),
			flex: 1,
			alignItems: 'center',
			justifyContent: 'center',
		}, style]}>
			<Image
				source={require('./../../assets/ic-check-circle.png')}
				style={{
					width: Common.getLengthByIPhone7(203),
					height: Common.getLengthByIPhone7(203),
					resizeMode: 'contain',
				}}
			/>
			<Text style={{
				fontFamily: 'Gilroy-SemiBold',
				fontSize: Common.getLengthByIPhone7(36),
				color: 'black',
				marginTop: Common.getLengthByIPhone7(135),
			}}>
				Заявка оформлена
			</Text>
			<Text style={{
				fontFamily: 'Gilroy-Medium',
				fontSize: Common.getLengthByIPhone7(18),
				color: colors.GRAY3_COLOR,
				marginTop: Common.getLengthByIPhone7(10),
			}}>
				Ваша анкета на рассмотрении
			</Text>
		</View>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(ModerationView);