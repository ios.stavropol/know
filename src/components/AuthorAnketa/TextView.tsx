import React, { useEffect } from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import { FloatingLabelInput } from 'react-native-floating-label-input';

const TextView = ({title, placeholder, max, onChange, style}) => {

	const [value, setValue] = React.useState('');

	useEffect(() => {
		if (onChange) {
			onChange(value);
		}
	}, [value]);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
			borderRadius: Common.getLengthByIPhone7(20),
			padding: Common.getLengthByIPhone7(25),
			backgroundColor: 'white',
			shadowColor: "black",
			shadowOffset: {
				width: 0,
				height: 0,
			},
			shadowOpacity: 0.05,
			shadowRadius: 32.00,
			elevation: 1,
		}, style]}>
			<Text style={{
				fontFamily: 'Gilroy-Bold',
				fontSize: Common.getLengthByIPhone7(18),
				color: colors.GRAY1_COLOR,
			}}>
				{title}
			</Text>
			<FloatingLabelInput
				label={placeholder}
				maxLength={max}
				value={value}
				onChangeText={text => setValue(text)}
				containerStyles={{
					marginTop: Common.getLengthByIPhone7(10),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
					height: Common.getLengthByIPhone7(50),
					paddingHorizontal: 0,
					backgroundColor: 'white',
					borderBottomColor: '#D7E0E7',
					borderBottomWidth: 1,
				}}
				customLabelStyles={{
					leftFocused: -4,
					leftBlurred: 0,
					topFocused: -20,
					fontSizeFocused: Common.getLengthByIPhone7(12),
					fontSizeBlurred: Common.getLengthByIPhone7(16),
					colorFocused: '#8FA4B2',
					colorBlurred: '#8FA4B2',
				}}
				labelStyles={{
					color: '#8FA4B2',
					fontSize: Common.getLengthByIPhone7(12),
					fontFamily: 'Gilroy-Regular',
					fontWeight: '400',
				}}
				inputStyles={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
					marginTop: Common.getLengthByIPhone7(5),
					color: '#172026',
					fontSize: Common.getLengthByIPhone7(16),
					fontFamily: 'Gilroy-Medium',
					fontWeight: '400',
				}}
			/>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
				fontFamily: 'Gilroy-Regular',
				fontSize: Common.getLengthByIPhone7(11),
				color: '#8FA4B2',
				textAlign: 'right',
				marginTop: 5,
			}}>
				{value.length}/{max}
			</Text>
		</View>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(TextView);