import React, { useEffect } from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

const PhotoView = ({onChange, style}) => {

	const [photo, setPhoto] = React.useState(null);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
			borderRadius: Common.getLengthByIPhone7(20),
			paddingLeft: Common.getLengthByIPhone7(27),
			paddingRight: Common.getLengthByIPhone7(27),
			paddingTop: Common.getLengthByIPhone7(25),
			paddingBottom: Common.getLengthByIPhone7(25),
			backgroundColor: 'white',
			shadowColor: "black",
			shadowOffset: {
				width: 0,
				height: 0,
			},
			shadowOpacity: 0.05,
			shadowRadius: 32.00,
			elevation: 1,
		}, style]}>
			<Text style={{
				fontFamily: 'Gilroy-Bold',
				fontSize: Common.getLengthByIPhone7(18),
				color: colors.GRAY1_COLOR,
			}}>
				Ваша фотография
			</Text>
			<Text style={{
				fontFamily: 'Gilroy-Regular',
				fontSize: Common.getLengthByIPhone7(14),
				marginTop: Common.getLengthByIPhone7(10),
				color: colors.GRAY3_COLOR,
			}}>
				Выбери и загрузи файл с устройства размером 350х350 p
			</Text>
			{photo === null ? (<TouchableOpacity style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
				height: Common.getLengthByIPhone7(90),
				borderRadius: Common.getLengthByIPhone7(10),
				marginTop: Common.getLengthByIPhone7(10),
				borderColor: colors.BLUE_COLOR,
				borderStyle: 'dashed',
				borderWidth: 1,
				alignItems: 'center',
				justifyContent: 'center',
			}}
			onPress={() => {
				setTimeout(() => {
					launchImageLibrary({
						mediaType: 'photo',
					})
					.then(response => {
						console.warn('response: ', response);
						if (response.assets?.length) {
							ImageResizer.createResizedImage(response.assets[0].uri, response.assets[0].width / 2, response.assets[0].height / 2, "JPEG", 100, 0)
							.then(response => {
								setPhoto(response.uri);
								if (onChange) {
									onChange(response.uri);
								}
							})
							.catch(err => {
								console.warn('createResizedImage err: ', err);
							});
						}
					})
					.catch(err => {
						console.warn('response err: ', err);
					});
				}, 1000);
			}}>
				<Image
					source={require('./../../assets/ic-upload.png')}
					style={{
						width: Common.getLengthByIPhone7(25),
						height: Common.getLengthByIPhone7(25),
						resizeMode: 'contain',
					}}
				/>
				<Text style={{
					fontFamily: 'Gilroy-Regular',
					fontSize: Common.getLengthByIPhone7(12),
					marginTop: Common.getLengthByIPhone7(10),
					color: colors.BLUE_COLOR,
				}}>
					Загрузить изображение
				</Text>
			</TouchableOpacity>) : (<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
				height: Common.getLengthByIPhone7(310),
				borderRadius: Common.getLengthByIPhone7(10),
				marginTop: Common.getLengthByIPhone7(10),
				overflow: 'hidden',
			}}>
				<Image
					source={{uri: photo}}
					style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
						height: Common.getLengthByIPhone7(310),
						resizeMode: 'cover',
					}}
				/>
				<TouchableOpacity style={{
					position: 'absolute',
					left: Common.getLengthByIPhone7(10),
					top: Common.getLengthByIPhone7(10),
				}}
				onPress={() => {
					setPhoto(null);
				}}>
					<Image
						source={require('./../../assets/ic-delete.png')}
						style={{
							width: Common.getLengthByIPhone7(25),
							height: Common.getLengthByIPhone7(25),
							resizeMode: 'contain',
						}}
					/>
				</TouchableOpacity>
			</View>)}
		</View>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(PhotoView);