import React, { useEffect } from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import { FloatingLabelInput } from 'react-native-floating-label-input';
import SocialItem from './SocialItem';

const SocialView = ({max, onChange, style}) => {

	const [socials, setSocials] = React.useState([
		{
			url: '',
			count: '',
		}
	]);
	const [body, setBody] = React.useState(null);

	useEffect(() => {
		if (onChange) {
			onChange(socials);
		}

		let array = [];
		for (let i = 0; i < socials.length; i++) {
			array.push(<SocialItem
				onChangeUrl={text => {
					let items = JSON.parse(JSON.stringify(socials));
					items[i].url = text;
					setSocials(items);
				}}
				onChangeFollowers={text => {
					let items = JSON.parse(JSON.stringify(socials));
					items[i].count = text;
					setSocials(items);
				}}
			/>);
		}
		setBody(array);
	}, [socials]);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
			borderRadius: Common.getLengthByIPhone7(20),
			padding: Common.getLengthByIPhone7(25),
			backgroundColor: 'white',
			shadowColor: "black",
			shadowOffset: {
				width: 0,
				height: 0,
			},
			shadowOpacity: 0.05,
			shadowRadius: 32.00,
			elevation: 1,
		}, style]}>
			<Text style={{
				fontFamily: 'Gilroy-Bold',
				fontSize: Common.getLengthByIPhone7(18),
				color: colors.GRAY1_COLOR,
			}}>
				Ссылка на Ваш профиль в соц. сети
			</Text>
			{body}
			<TouchableOpacity style={{
				marginTop: Common.getLengthByIPhone7(30),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
				alignItems: 'center',
			}}
			onPress={() => {
				let array = JSON.parse(JSON.stringify(socials));
				array.push({
					url: '',
					count: '',
				});
				setSocials(array);
			}}>
				<Text style={{
					color: '#8962D9',
					fontSize: Common.getLengthByIPhone7(13),
					fontFamily: 'Gilroy-Regular',
					fontWeight: '400',
				}}>
					Добавить социальную сеть
				</Text>
			</TouchableOpacity>
		</View>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(SocialView);