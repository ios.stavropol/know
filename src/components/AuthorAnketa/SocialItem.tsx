import React, { useEffect } from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import { FloatingLabelInput } from 'react-native-floating-label-input';

const SocialItem = ({max, onChangeUrl, onChangeFollowers, style}) => {

	const [url, setUrl] = React.useState('');
	const [followers, setFollowers] = React.useState('');

	useEffect(() => {
		if (onChangeUrl) {
			onChangeUrl(url);
		}
	}, [url]);

	useEffect(() => {
		if (onChangeFollowers) {
			onChangeFollowers(followers);
		}
	}, [followers]);

	return (
		<View style={{

		}}>
			<FloatingLabelInput
				label={'URL-адрес'}
				maxLength={50}
				value={url}
				onChangeText={text => setUrl(text)}
				keyboardType={'email-address'}
				containerStyles={{
					marginTop: Common.getLengthByIPhone7(10),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
					height: Common.getLengthByIPhone7(50),
					paddingHorizontal: 0,
					backgroundColor: 'white',
					borderBottomColor: '#D7E0E7',
					borderBottomWidth: 1,
				}}
				customLabelStyles={{
					leftFocused: -4,
					leftBlurred: 0,
					topFocused: -20,
					fontSizeFocused: Common.getLengthByIPhone7(12),
					fontSizeBlurred: Common.getLengthByIPhone7(16),
					colorFocused: '#8FA4B2',
					colorBlurred: '#8FA4B2',
				}}
				labelStyles={{
					color: '#8FA4B2',
					fontSize: Common.getLengthByIPhone7(12),
					fontFamily: 'Gilroy-Regular',
					fontWeight: '400',
				}}
				inputStyles={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
					marginTop: Common.getLengthByIPhone7(5),
					color: '#172026',
					fontSize: Common.getLengthByIPhone7(16),
					fontFamily: 'Gilroy-Medium',
					fontWeight: '400',
				}}
			/>
			<FloatingLabelInput
				label={'Кол-во подписчиков'}
				maxLength={50}
				value={followers}
				onChangeText={text => setFollowers(text)}
				keyboardType={'number-pad'}
				containerStyles={{
					marginTop: Common.getLengthByIPhone7(10),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
					height: Common.getLengthByIPhone7(50),
					paddingHorizontal: 0,
					backgroundColor: 'white',
					borderBottomColor: '#D7E0E7',
					borderBottomWidth: 1,
				}}
				customLabelStyles={{
					leftFocused: -4,
					leftBlurred: 0,
					topFocused: -20,
					fontSizeFocused: Common.getLengthByIPhone7(12),
					fontSizeBlurred: Common.getLengthByIPhone7(16),
					colorFocused: '#8FA4B2',
					colorBlurred: '#8FA4B2',
				}}
				labelStyles={{
					color: '#8FA4B2',
					fontSize: Common.getLengthByIPhone7(12),
					fontFamily: 'Gilroy-Regular',
					fontWeight: '400',
				}}
				inputStyles={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
					marginTop: Common.getLengthByIPhone7(5),
					color: '#172026',
					fontSize: Common.getLengthByIPhone7(16),
					fontFamily: 'Gilroy-Medium',
					fontWeight: '400',
				}}
			/>
		</View>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(SocialItem);