import React, { useEffect } from 'react';
import {TouchableOpacity, View, TextInput, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';

const AboutView = ({max, onChange, style}) => {

	const [about, setAbout] = React.useState('');

	useEffect(() => {
		if (onChange) {
			onChange(about);
		}
	}, [about]);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
			borderRadius: Common.getLengthByIPhone7(20),
			padding: Common.getLengthByIPhone7(25),
			backgroundColor: 'white',
			shadowColor: "black",
			shadowOffset: {
				width: 0,
				height: 0,
			},
			shadowOpacity: 0.05,
			shadowRadius: 32.00,
			elevation: 1,
		}, style]}>
			<Text style={{
				fontFamily: 'Gilroy-Bold',
				fontSize: Common.getLengthByIPhone7(18),
				color: colors.GRAY1_COLOR,
			}}>
				Пару слов о себе
			</Text>
			<TextInput
				style={{
					marginTop: Common.getLengthByIPhone7(10),
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
					height: Common.getLengthByIPhone7(140),
					borderRadius: Common.getLengthByIPhone7(10),
					backgroundColor: 'white',
					color: '#172026',
					fontSize: Common.getLengthByIPhone7(16),
					fontFamily: 'Gilroy-Regular',
					fontWeight: '400',
					textAlign: 'left',
				}}
				multiline
				maxLength={300}
				placeholderTextColor={'#8FA4B2'}
				placeholder={'Расскажите коротко о себе и, о направлении ваших курсов'}
				returnKeyType={'done'}
				allowFontScaling={false}
				underlineColorAndroid={'transparent'}
				onSubmitEditing={() => {
					// this.nextClick();
				}}
				onFocus={() => {}}
				onBlur={() => {
				
				}}
				onChangeText={code => {
					setAbout(code);
				}}
				value={about}
			/>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
				fontFamily: 'Gilroy-Regular',
				fontSize: Common.getLengthByIPhone7(11),
				color: '#8FA4B2',
				textAlign: 'right',
				marginTop: 5,
			}}>
				{about.length}/300
			</Text>
		</View>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(AboutView);