import React, { useEffect } from 'react';
import {View, Text} from 'react-native';
import Common from '../utilities/Common';
import { colors } from '../styles';
import { typography } from '../styles';
import { isIphoneX } from 'react-native-iphone-x-helper';

const NavBarCentr = ({title, headerLeft, headerRight}) => {

	return (
		<View style={{
			width: Common.getLengthByIPhone7(0),
			height: isIphoneX() ? 100 : 70,
			alignItems: 'flex-end',
			justifyContent: 'space-between',
			flexDirection: 'row',
			backgroundColor: colors.WHITE_COLOR,
			paddingBottom: 25,
			zIndex: 20,
		}}>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'center',
				// height: Common.getLengthByIPhone7(36),
			}}>
				<View style={{
					position: 'absolute',
					left: 0,
				}}>
					{headerLeft}
				</View>
				<Text style={[{
					color: colors.BLACK_COLOR,
				}]}
				allowFontScaling={false}>
					{title}
				</Text>
				<View style={{
					position: 'absolute',
					right: 0,
				}}>
					{headerRight}
				</View>
			</View>
		</View>
	);
};

export default NavBarCentr;