import React, { useEffect } from 'react';
import {TouchableOpacity, View, TextInput, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import { Switch } from 'react-native-switch';

const AuthorView = ({onChange, data, style}) => {

	const [value, setValue] = React.useState(false);

	useEffect(() => {
		if (onChange) {
			onChange(value);
		}
	}, [value]);

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
			paddingBottom: Common.getLengthByIPhone7(14),
			backgroundColor: 'white',
			borderBottomColor: colors.GRAY4_COLOR,
			borderBottomWidth: 1,
		}, style]}>
			<View style={{
				height: Common.getLengthByIPhone7(40),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
			}}>
				<Text style={{
					fontFamily: 'Gilroy-Bold',
					fontSize: Common.getLengthByIPhone7(18),
					color: colors.GRAY2_COLOR,
				}}>
					{data.name}
				</Text>
				<Text style={{
					fontFamily: 'Gilroy-Regular',
					fontSize: Common.getLengthByIPhone7(13),
					color: colors.GRAY3_COLOR,
				}}>
					Автопродление
				</Text>
			</View>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
			}}>
				<Text style={{
					fontFamily: 'Gilroy-Bold',
					fontSize: Common.getLengthByIPhone7(15),
					color: colors.GRAY_COLOR,
				}}>
					{Common.getDigitStr(data.period, {
						d1: ' месяц',
						d2_d4: ' месяца',
						d5_d10: ' месяцев',
						d11_d19: ' месяцев',
					})}: {data.price} ₽
				</Text>
				<View style={{
					width: Common.getLengthByIPhone7(48),
					height: Common.getLengthByIPhone7(25),
					borderRadius: Common.getLengthByIPhone7(24),
					alignItems: 'center',
					justifyContent: 'center',
					overflow: 'hidden',
					borderColor: value ? colors.BLUE_COLOR : colors.GRAY4_COLOR,
					borderWidth: 2,
				}}>
					<Switch
						value={value}
						onValueChange={val => setValue(val)}
						activeText={'On'}
						inActiveText={'Off'}
						circleSize={Common.getLengthByIPhone7(18)}
						barHeight={Common.getLengthByIPhone7(24)}
						circleBorderWidth={0}
						backgroundActive={'white'}
						backgroundInactive={'white'}
						circleActiveColor={colors.BLUE_COLOR}
						circleInActiveColor={colors.GRAY4_COLOR}
						changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
						innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
						renderActiveText={false}
						renderInActiveText={false}
						switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
						switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
						switchWidthMultiplier={2.5}
						switchBorderRadius={Common.getLengthByIPhone7(24)}
					/>
				</View>
			</View>
		</View>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(AuthorView);