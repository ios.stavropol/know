import React, { useEffect } from 'react';
import {TouchableOpacity, View, Image, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';
import { Switch } from 'react-native-switch';

const DiskView = ({onChange, data, style}) => {

	const [value, setValue] = React.useState(false);
	const [selected, setSelected] = React.useState(0);
	const [body, setBody] = React.useState(null);

	useEffect(() => {
		let array = {};
		let array2 = data;
		array2 = array2.sort((a, b) => {
			return a.period > b.period;
		});
		for (let i = 0; i < array2.length; i++) {
			if (!array[array2[i].volume]) {
				array[array2[i].volume] = [];
			}
			array[array2[i].volume].push(array2[i]);
		}

		let keys = Object.keys(array);
		array2 = [];
		for (let i = 0; i < keys.length; i++) {
			array2.push(renderRow(array[keys[i]]));
		}
		setBody(array2);
	}, [data, selected]);

	const renderRow = items => {
		return (<View style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
			marginTop: Common.getLengthByIPhone7(23),
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			<View style={{
				width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30)) / 4,
				alignItems: 'flex-start',
			}}>
				<Text style={{
					fontFamily: 'Gilroy-Medium',
					fontSize: Common.getLengthByIPhone7(16),
					color: colors.GRAY2_COLOR,
				}}>
					{items[0].name}
				</Text>
			</View>
			<View style={{
				width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30)) / 4,
				alignItems: 'center',
			}}>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(74),
					height: Common.getLengthByIPhone7(30),
					borderRadius: Common.getLengthByIPhone7(25),
					borderColor: selected === items[0].id ? colors.BLUE_COLOR : colors.GRAY3_COLOR,
					backgroundColor: selected === items[0].id ? colors.BLUE_COLOR : 'white',
					borderWidth: 1,
					alignItems: 'center',
					justifyContent: 'center',
				}}
				activeOpacity={1}
				onPress={() => {
					setSelected(items[0].id);
				}}>
					<Text style={{
						fontFamily: 'Gilroy-Bold',
						fontSize: Common.getLengthByIPhone7(14),
						color: selected === items[0].id ? 'white' : colors.GRAY3_COLOR,
					}}>
						{items[0].price} ₽
					</Text>
				</TouchableOpacity>
			</View>
			<View style={{
				width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30)) / 4,
				alignItems: 'center',
			}}>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(74),
					height: Common.getLengthByIPhone7(30),
					borderRadius: Common.getLengthByIPhone7(25),
					borderColor: selected === items[1].id ? colors.BLUE_COLOR : colors.GRAY3_COLOR,
					backgroundColor: selected === items[1].id ? colors.BLUE_COLOR : 'white',
					borderWidth: 1,
					alignItems: 'center',
					justifyContent: 'center',
				}}
				activeOpacity={1}
				onPress={() => {
					setSelected(items[1].id);
				}}>
					<Text style={{
						fontFamily: 'Gilroy-Bold',
						fontSize: Common.getLengthByIPhone7(14),
						color: selected === items[1].id ? 'white' : colors.GRAY3_COLOR,
					}}>
						{items[1].price} ₽
					</Text>
				</TouchableOpacity>
			</View>
			<View style={{
				width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30)) / 4,
				alignItems: 'center',
			}}>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(74),
					height: Common.getLengthByIPhone7(30),
					borderRadius: Common.getLengthByIPhone7(25),
					borderColor: selected === items[2].id ? colors.BLUE_COLOR : colors.GRAY3_COLOR,
					backgroundColor: selected === items[2].id ? colors.BLUE_COLOR : 'white',
					borderWidth: 1,
					alignItems: 'center',
					justifyContent: 'center',
				}}
				activeOpacity={1}
				onPress={() => {
					setSelected(items[2].id);
				}}>
					<Text style={{
						fontFamily: 'Gilroy-Bold',
						fontSize: Common.getLengthByIPhone7(14),
						color: selected === items[2].id ? 'white' : colors.GRAY3_COLOR,
					}}>
						{items[2].price} ₽
					</Text>
				</TouchableOpacity>
			</View>
		</View>);
	}

	return (
		<View style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
			paddingBottom: Common.getLengthByIPhone7(14),
			backgroundColor: 'white',
			borderBottomColor: colors.GRAY4_COLOR,
			borderBottomWidth: 1,
		}, style]}>
			<View style={{
				height: Common.getLengthByIPhone7(40),
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
				flexDirection: 'row',
				alignItems: 'center',
				justifyContent: 'space-between',
			}}>
				<Text style={{
					fontFamily: 'Gilroy-Bold',
					fontSize: Common.getLengthByIPhone7(18),
					color: colors.GRAY2_COLOR,
				}}>
					Место для хранения
				</Text>
				<View style={{
					width: Common.getLengthByIPhone7(48),
					height: Common.getLengthByIPhone7(25),
					borderRadius: Common.getLengthByIPhone7(24),
					alignItems: 'center',
					justifyContent: 'center',
					overflow: 'hidden',
					borderColor: value ? colors.BLUE_COLOR : colors.GRAY4_COLOR,
					borderWidth: 2,
				}}>
					<Switch
						value={value}
						onValueChange={val => setValue(val)}
						activeText={'On'}
						inActiveText={'Off'}
						circleSize={Common.getLengthByIPhone7(18)}
						barHeight={Common.getLengthByIPhone7(24)}
						circleBorderWidth={0}
						backgroundActive={'white'}
						backgroundInactive={'white'}
						circleActiveColor={colors.BLUE_COLOR}
						circleInActiveColor={colors.GRAY4_COLOR}
						changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
						innerCircleStyle={{ alignItems: "center", justifyContent: "center" }} // style for inner animated circle for what you (may) be rendering inside the circle
						renderActiveText={false}
						renderInActiveText={false}
						switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
						switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
						switchWidthMultiplier={2.5}
						switchBorderRadius={Common.getLengthByIPhone7(24)}
					/>
				</View>
			</View>
			<View style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
				marginTop: Common.getLengthByIPhone7(23),
				flexDirection: 'row',
				alignItems: 'flex-end',
				justifyContent: 'space-between',
			}}>
				<View style={{
					width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30)) / 4,
					alignItems: 'flex-start',
				}}>
					<Image
						source={require('./../../assets/ic-volume.png')}
						style={{
							width: Common.getLengthByIPhone7(57),
							height: Common.getLengthByIPhone7(18),
							resizeMode: 'contain',
						}}
					/>
				</View>
				<View style={{
					width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30)) / 4,
					alignItems: 'center',
				}}>
					<Image
						source={require('./../../assets/ic-new.png')}
						style={{
							width: Common.getLengthByIPhone7(64),
							height: Common.getLengthByIPhone7(70),
							resizeMode: 'contain',
						}}
					/>
				</View>
				<View style={{
					width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30)) / 4,
					alignItems: 'center',
				}}>
					<Image
						source={require('./../../assets/ic-expert.png')}
						style={{
							width: Common.getLengthByIPhone7(80),
							height: Common.getLengthByIPhone7(70),
							resizeMode: 'contain',
						}}
					/>
				</View>
				<View style={{
					width: (Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30)) / 4,
					alignItems: 'center',
				}}>
					<Image
						source={require('./../../assets/ic-profi.png')}
						style={{
							width: Common.getLengthByIPhone7(80),
							height: Common.getLengthByIPhone7(70),
							resizeMode: 'contain',
						}}
					/>
				</View>
			</View>
			{body}
		</View>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(DiskView);