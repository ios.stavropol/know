import React, { useCallback, useEffect, useRef } from 'react';
import { Image, StatusBar, View, TouchableOpacity, Linking, Text, Alert } from 'react-native';
import { RootState, Dispatch } from './../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Common from '../utilities/Common';
import { colors, typography } from './../styles';

const BlueButton = ({title, disabled, onPress, style}) => {

	return (
        <TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(64),
			height: Common.getLengthByIPhone7(60),
			borderRadius: Common.getLengthByIPhone7(5),
			alignItems: 'center',
			justifyContent: 'center',
			backgroundColor: colors.BLUE_COLOR,
			opacity: disabled ? 0.5 : 1,
		}, style]}
		activeOpacity={disabled ? 1 : 0.7}
		onPress={() => {
			if (onPress) {
				onPress();
			}
		}}>
			<Text style={[{
				fontFamily: 'Gilroy-SemiBold',
				fontSize: Common.getLengthByIPhone7(20),
				color: colors.WHITE_COLOR,
			}]}
			allowFontScaling={false}>
				{title}
			</Text>
		</TouchableOpacity>
	);
};

export default BlueButton;
