import React, { useEffect } from 'react';
import {TouchableOpacity, Text} from 'react-native';
import Common from './../../utilities/Common';
import { useNavigation } from '@react-navigation/native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import { colors } from './../../styles';

const SkipButton = ({navigation, skipName, style}) => {

	return (
		<TouchableOpacity style={[{
			// width: Common.getLengthByIPhone7(32),
			// height: Common.getLengthByIPhone7(32),
			// alignItems: 'flex-start',
			// justifyContent: 'center',
		}, style]}
		onPress={() => {
			if (skipName) {
				skipName();
			}
		}}>
			<Text style={{
				fontFamily: 'Gilroy-Regular',
				fontSize: Common.getLengthByIPhone7(18),
				color: colors.GRAY3_COLOR,
			}}>
				Пропустить
			</Text>
		</TouchableOpacity>
	);
};

const mstp = (state: RootState) => ({
	skipName: state.buttons.skipName,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(SkipButton);