import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import { Image, Platform } from 'react-native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { colors } from '../styles';
import { typography } from '../styles';
import Splash2Screen from '../screens/Splash2';
import EnterScreen from '../screens/Enter';
import EnterEmailScreen from '../screens/EnterEmail';
import EnterCodeScreen from '../screens/EnterCode';
import EnterNameScreen from '../screens/EnterName';
import SelectCategoriesScreen from '../screens/SelectCategories';
import RulesScreen from '../screens/Rules';
import RuleViewScreen from '../screens/RuleView';
import MainScreen from '../screens/Main';
import ChatsScreen from '../screens/Chats';
import ProfileScreen from '../screens/Profile';
import CreateScreen from '../screens/Create';
import AuthorAnketaScreen from '../screens/AuthorAnketa';
import CategoryModalScreen from '../screens/CategoryModal';
import ActivateAuthorScreen from '../screens/ActivateAuthor';
import RekvizitsScreen from '../screens/Rekvizits';
import RekvizitsSavedScreen from '../screens/RekvizitsSaved';

import common from './../utilities/Common';

import BackButton from '../components/BackButton';
import SkipButton from '../components/EnterName/SkipButton';
import NavBarCentr from '../components/NavBarCentr';
import Tabbar from '../components/Tabbar';

export const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const EnterStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'Splash2'}
        component={Splash2Screen}
        options={{
            headerShown: false,
        }}
      />
      <Stack.Screen
        name={'Enter'}
        component={EnterScreen}
        options={{
          headerShown: false,
          animation: 'fade',
          presentation: 'card',
          gestureEnabled: false,
          headerBackVisible: false,
          headerShadowVisible: false,
        }}
      />
      <Stack.Screen
        name={'EnterEmail'}
        component={EnterEmailScreen}
        options={{
          headerShown: true,
          gestureEnabled: true,
          headerShadowVisible: false,
          headerStyle: {
            backgroundColor: colors.LIGHT_GRAY_COLOR,
          },
          headerTitleAlign: 'center',
          header: (props) => (
            <NavBarCentr
              {...props}
              title={''}
              headerLeft={ <BackButton {...props} style={{
                marginLeft: common.getLengthByIPhone7(17),
              }}/>}
            />
          ),
          headerTitleStyle: [typography.H4_R16_140, {
            color: colors.BLUE_COLOR,
          }]
        }}
      />
      <Stack.Screen
        name={'EnterCode'}
        component={EnterCodeScreen}
        options={{
          headerShown: true,
          gestureEnabled: true,
          headerShadowVisible: false,
          headerStyle: {
            backgroundColor: colors.LIGHT_GRAY_COLOR,
          },
          headerTitleAlign: 'center',
          header: (props) => (
            <NavBarCentr
              {...props}
              title={''}
              headerLeft={ <BackButton {...props} style={{
                marginLeft: common.getLengthByIPhone7(17),
              }}/>}
            />
          ),
          headerTitleStyle: [typography.H4_R16_140, {
            color: colors.BLUE_COLOR,
          }]
        }}
      />
      <Stack.Screen
        name={'Rules'}
        component={RulesScreen}
        options={{
          headerShown: true,
          gestureEnabled: true,
          headerShadowVisible: false,
          headerStyle: {
            backgroundColor: colors.LIGHT_GRAY_COLOR,
          },
          headerTitleAlign: 'center',
          header: (props) => (
            <NavBarCentr
              {...props}
              title={''}
              headerLeft={ <BackButton {...props} style={{
                marginLeft: common.getLengthByIPhone7(17),
              }}/>}
            />
          ),
          headerTitleStyle: [typography.H4_R16_140, {
            color: colors.BLUE_COLOR,
          }]
        }}
      />
      <Stack.Screen
        name={'RuleView'}
        component={RuleViewScreen}
        options={{
          headerShown: true,
          gestureEnabled: true,
          headerShadowVisible: false,
          headerStyle: {
            backgroundColor: colors.LIGHT_GRAY_COLOR,
          },
          headerTitleAlign: 'center',
          header: (props) => (
            <NavBarCentr
              {...props}
              title={''}
              headerLeft={ <BackButton {...props} style={{
                marginLeft: common.getLengthByIPhone7(17),
              }}/>}
            />
          ),
          headerTitleStyle: [typography.H4_R16_140, {
            color: colors.BLUE_COLOR,
          }]
        }}
      />
    </Stack.Navigator>
  );
}

const SelfSaveStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'EnterName'}
        component={EnterNameScreen}
        options={{
          headerShown: true,
          animation: 'fade',
          presentation: 'card',
          gestureEnabled: false,
          headerBackVisible: false,
          headerShadowVisible: false,
          header: (props) => (
            <NavBarCentr
              {...props}
              title={''}
              headerRight={ <SkipButton {...props} style={{
                marginRight: common.getLengthByIPhone7(17),
              }}/>}
            />
          ),
        }}
      />
      <Stack.Screen
        name={'SelectCategories'}
        component={SelectCategoriesScreen}
        options={{
          headerShown: true,
          gestureEnabled: true,
          headerBackVisible: false,
          headerShadowVisible: false,
          header: (props) => (
            <NavBarCentr
              {...props}
              title={''}
              headerLeft={ <BackButton {...props} style={{
                marginLeft: common.getLengthByIPhone7(17),
              }}/>}
              headerRight={ <SkipButton {...props} style={{
                marginRight: common.getLengthByIPhone7(17),
              }}/>}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
}

const MainTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Main'} component={MainScreen} options={{
            headerTransparent: false,
            headerShown: true,
            gestureEnabled: false,
            headerBackVisible: false,
            headerShadowVisible: false,
          }}/>
      </Stack.Navigator>
  );
}

const ChatTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Chats'} component={ChatsScreen} options={{
            headerTransparent: false,
            headerShown: true,
            gestureEnabled: false,
            headerBackVisible: false,
            headerShadowVisible: false,
          }}/>
      </Stack.Navigator>
  );
}

const ProfileTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Profile'} component={ProfileScreen} options={{
            headerTransparent: false,
            headerShown: true,
            gestureEnabled: false,
            headerBackVisible: false,
            headerShadowVisible: false,
          }}/>
      </Stack.Navigator>
  );
}

const CreateTab = () => {
  return (
      <Stack.Navigator>
          <Stack.Screen name={'Create'} component={CreateScreen} options={{
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen
            name={'AuthorAnketa'}
            component={AuthorAnketaScreen}
            options={{
              headerShown: true,
              gestureEnabled: true,
              headerBackVisible: false,
              headerShadowVisible: false,
              header: (props) => (
                <NavBarCentr
                  {...props}
                  title={''}
                  headerLeft={ <BackButton {...props} style={{
                    marginLeft: common.getLengthByIPhone7(17),
                  }}/>}
                />
              ),
            }}
          />
          <Stack.Screen
            name={'ActivateAuthor'}
            component={ActivateAuthorScreen}
            options={{
              headerShown: true,
              gestureEnabled: true,
              headerBackVisible: false,
              headerShadowVisible: false,
              header: (props) => (
                <NavBarCentr
                  {...props}
                  title={''}
                  headerLeft={ <BackButton {...props} style={{
                    marginLeft: common.getLengthByIPhone7(17),
                  }}/>}
                />
              ),
            }}
          />
          <Stack.Screen
            name={'Rekvizits'}
            component={RekvizitsScreen}
            options={{
              headerShown: true,
              gestureEnabled: true,
              headerBackVisible: false,
              headerShadowVisible: false,
              header: (props) => (
                <NavBarCentr
                  {...props}
                  title={''}
                  headerLeft={ <BackButton {...props} style={{
                    marginLeft: common.getLengthByIPhone7(17),
                  }}/>}
                />
              ),
            }}
          />
          <Stack.Screen
            name={'RekvizitsSaved'}
            component={RekvizitsSavedScreen}
            options={{
              headerShown: false,
              gestureEnabled: false,
              headerBackVisible: false,
              headerShadowVisible: false,
            }}
          />
      </Stack.Navigator>
  );
}

const Tabs = () => {
  return (
      <Tab.Navigator
        tabBar={props => <Tabbar {...props} />}
        screenOptions={({ route }) => ({
          lazy: false,
          tabBarStyle: {
            backgroundColor: 'white',
            borderTopWidth: 0,
            shadowColor: '#CED6E8',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 1,
            shadowRadius: 10,
            elevation: 4,
          },
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'MainTab') {
              iconName = require('./../assets/ic-tab1.png');
            } else if (route.name === 'ChatTab') {
              iconName = require('./../assets/ic-tab2.png');
            } else if (route.name === 'ProfileTab') {
              iconName = require('./../assets/ic-tab3.png');
            } else if (route.name === 'CreateTab') {
              iconName = require('./../assets/ic-tab4.png');
            }

            return (<Image
              source={iconName}
              style={{
                marginBottom: Platform.OS === 'ios' ? (isIphoneX() ? -10 : 0) : 0,
                resizeMode: 'contain',
                width: 24, 
                height: 24,
                tintColor: color,
              }}
            />);
          },
          tabBarActiveTintColor: colors.BLACK_COLOR,
          tabBarInactiveTintColor: colors.GRAY_COLOR,
        })}>
          <Tab.Screen name={'MainTab'} component={MainTab} options={{
              tabBarShowLabel: false,
              headerShown: false,
          }}/>
          <Tab.Screen name={'ChatTab'} component={ChatTab} options={{
              tabBarShowLabel: false,
              headerShown: false,
          }}/>
          <Tab.Screen name={'ProfileTab'} component={ProfileTab} options={{
              tabBarShowLabel: false,
              headerShown: false,
          }}/>
          <Tab.Screen name={'CreateTab'} component={CreateTab} options={{
              tabBarShowLabel: false,
              headerShown: false,
          }}/>
      </Tab.Navigator>
  );
}

export default Router = () => {
  return (
    <NavigationContainer>
      <SafeAreaProvider>
        <Stack.Navigator>
          <Stack.Screen
            name={'Splash'}
            component={EnterStack}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name={'SelfSave'}
            component={SelfSaveStack}
            options={{
              gestureEnabled: false,
              headerShown: false,
              animation: 'fade',
              presentation: 'card',
            }}
          />
          <Stack.Screen name={'LoginIn'} component={Tabs} options={{
            animation: 'slide_from_bottom',
            presentation: 'card',
            headerShown: false,
            gestureEnabled: false,
          }}/>
          <Stack.Screen name={'CategoryModal'} component={CategoryModalScreen} options={{
            animation: 'fade',
            presentation: 'card',
            headerShown: false,
            gestureEnabled: false,
            contentStyle: {
              backgroundColor: "transparent"
            },
            cardStyle: { backgroundColor: 'transparent' },
            presentation: 'transparentModal',
          }}/>
        </Stack.Navigator>
      </SafeAreaProvider>
    </NavigationContainer>
  );
}
