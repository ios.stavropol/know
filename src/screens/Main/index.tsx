import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Platform, View, TextInput, TouchableOpacity, Text, StatusBar } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const MainScreen = ({getCode}) => {

	const navigation = useNavigation();

	useEffect(() => {
		if (Platform.OS === 'ios') {
			StatusBar.setBarStyle('dark-content', true);
		} else {
			StatusBar.setBarStyle('dark-content', true);
			// StatusBar.setBackgroundColor(colors.BLUE_COLOR, true);
		}
	}, []);

	return (
		<View style={{
			flex: 1,
			backgroundColor: colors.BACKGROUND_COLOR,
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			
		</View>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(MainScreen);
