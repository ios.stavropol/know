import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Platform, View, TextInput, TouchableOpacity, Text, StatusBar } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import BlueButton from '../../components/BlueButton';
import { VERSION } from '../../constants';

const EnterEmailScreen = ({getCode}) => {

	const navigation = useNavigation();

	const input = useRef(null);
	const [email, setEmail] = React.useState('');
	const [showError, setShowError] = React.useState(0);

	useEffect(() => {
		if (Platform.OS === 'ios') {
			StatusBar.setBarStyle('dark-content', true);
		} else {
			StatusBar.setBarStyle('dark-content', true);
			StatusBar.setBackgroundColor(colors.WHITE_COLOR, true);
		}
	}, []);

	const next = () => {
		getCode(email)
		.then(() => {
			navigation.navigate('EnterCode', {email: email});
		})
		.catch(err => {

		});
	}

	return (
		<View style={{
			flex: 1,
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			<View>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(34),
					fontFamily: 'Gilroy-Light',
					fontSize: Common.getLengthByIPhone7(20),
					lineHeight: Common.getLengthByIPhone7(30),
					color: colors.GRAY3_COLOR,
				}}
				allowFontScaling={false}>
					Добро пожаловать
				</Text>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(34),
					fontFamily: 'Gilroy-Bold',
					fontSize: Common.getLengthByIPhone7(30),
					lineHeight: Common.getLengthByIPhone7(36),
					color: '#333333',
					marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					Введите адрес электронной почты
				</Text>
				<TextInput
					style={[{
						marginTop: Common.getLengthByIPhone7(80),
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
						minHeight: Common.getLengthByIPhone7(65),
						borderRadius: Common.getLengthByIPhone7(12),
						backgroundColor: '#f4f5f6',
						textAlign: 'center',
						fontFamily: 'Gilroy-Regular',
						fontSize: Common.getLengthByIPhone7(30),
						paddingLeft: Common.getLengthByIPhone7(10),
						paddingRight: Common.getLengthByIPhone7(10),
						color: showError === 0 ? colors.BLACK_COLOR : (showError === 1 ? colors.GREEN1_COLOR : colors.RED_COLOR),
					}]}
					autoCapitalize={'none'}
					multiline={false}
					numberOfLines={1}
					autoFocus
					ref={input}
					placeholderTextColor={colors.GRAY4_COLOR}
					placeholder={'usermail@mail.ru'}
					contextMenuHidden={false}
					autoCorrect={false}
					autoCompleteType={'off'}
					returnKeyType={'done'}
					secureTextEntry={false}
					keyboardType={'email-address'}
					allowFontScaling={false}
					underlineColorAndroid={'transparent'}
					onChangeText={text => {
						setEmail(text);
						setShowError(0);
					}}
					onBlur={() => {
						if (email?.length) {
							if (Common.validMail(email)) {
								setShowError(1);
								next();
							} else {
								setShowError(-1);
							}
						} else {
							setShowError(0);
						}
					}}
					value={email}
				/>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
					fontFamily: 'Gilroy-Light',
					fontSize: Common.getLengthByIPhone7(15),
					lineHeight: Common.getLengthByIPhone7(18),
					color: colors.RED_COLOR,
					textAlign: 'center',
					marginTop: Common.getLengthByIPhone7(15),
					opacity: showError === -1 ? 1 : 0,
				}}
				allowFontScaling={false}>
					Некорректный формат почты
				</Text>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
					fontFamily: 'Gilroy-Light',
					fontSize: Common.getLengthByIPhone7(15),
					lineHeight: Common.getLengthByIPhone7(18),
					color: colors.GRAY3_COLOR,
					textAlign: 'center',
					marginTop: Common.getLengthByIPhone7(15),
				}}
				allowFontScaling={false}>
					Продолжая авторизацию, вы соглашаетесь с <Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
					fontFamily: 'Gilroy-Medium',
					fontSize: Common.getLengthByIPhone7(15),
					lineHeight: Common.getLengthByIPhone7(18),
					color: colors.BLUE_COLOR,
					textAlign: 'center',
					// textDecorationLine: 'underline',
				}}
				allowFontScaling={false}
				onPress={() => {
					navigation.navigate('Rules');
				}}>
					пользовательским соглашением
				</Text>
				</Text>
			</View>
			{showError === 1 ? (<BlueButton
				style={{
					marginBottom: Common.getLengthByIPhone7(50),
				}}
				title={'Готово'}
				onPress={() => {
					next();
				}}
			/>) : null}
		</View>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	getCode: (payload) => dispatch.user.getCode(payload),
});

export default connect(mstp, mdtp)(EnterEmailScreen);
