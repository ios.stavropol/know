import React, { useCallback, useEffect, useRef } from 'react';
import { Platform, View, ScrollView, FlatList, TouchableOpacity, Text, Image, StatusBar, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import BlueButton from '../../components/BlueButton';
import { APP_NAME } from '../../constants';
import TextView from '../../components/Rekvizits/TextView';

const RekvizitsScreen = ({route, saveSelf, dictionaries, setSkipName}) => {

	const navigation = useNavigation();

	const [selected, setSelected] = React.useState([]);

	useEffect(() => {
		setSkipName(skipFun);
		if (Platform.OS === 'ios') {
			StatusBar.setBarStyle('dark-content', true);
		} else {
			StatusBar.setBarStyle('dark-content', true);
			StatusBar.setBackgroundColor(colors.WHITE_COLOR, true);
		}
	}, []);

	const skipFun = () => {
		saveSelf({
			name: '',
			category_ids: [],
		})
		.then(() => {
			navigation.navigate('LoginIn');
		})
		.catch(err => {
			Alert.alert(APP_NAME, err);
		});
	}

	const next = () => {
		saveSelf({
			name: route?.params?.name,
			category_ids: selected,
		})
		.then(() => {
			navigation.navigate('LoginIn');
		})
		.catch(err => {
			Alert.alert(APP_NAME, err);
		});
	}

	const renderRow = (item: object, index: number) => {
		return (<TouchableOpacity style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
			paddingTop: Common.getLengthByIPhone7(10),
			paddingBottom: Common.getLengthByIPhone7(10),
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}
		onPress={() => {
			let array = JSON.parse(JSON.stringify(selected));
			if (array.includes(item?.id)) {
				for (let i = 0; i < array.length; i++) {
					if (array[i] === item?.id) {
						array.splice(i, 1);
						break;
					}
				}
			} else {
				if (array.length < 3) {
					array.push(item?.id);
				} else {
					
				}
			}
			setSelected(array);
		}}>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(100),
				fontFamily: selected.includes(item?.id) ? 'Gilroy-SemiBold' : 'Gilroy-Regular',
				fontSize: Common.getLengthByIPhone7(20),
				lineHeight: Common.getLengthByIPhone7(30),
				color: colors.GRAY2_COLOR,
			}}
			allowFontScaling={false}>
				{item?.name}
			</Text>
			{selected.includes(item?.id) ? (<View style={{
				width: Common.getLengthByIPhone7(20),
				height: Common.getLengthByIPhone7(20),
				borderRadius: Common.getLengthByIPhone7(10),
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: colors.BLUE_COLOR,
			}}>
				<Image
					source={require('./../../assets/ic-check.png')}
					style={{
						width: Common.getLengthByIPhone7(12),
						height: Common.getLengthByIPhone7(12),
						resizeMode: 'contain',
					}}
				/>
			</View>) : (<View style={{
				width: Common.getLengthByIPhone7(20),
				height: Common.getLengthByIPhone7(20),
				borderRadius: Common.getLengthByIPhone7(10),
				alignItems: 'center',
				justifyContent: 'center',
				borderColor: colors.BLUE_COLOR,
				borderWidth: 2,
				opacity: 0.5,
			}}/>)}
		</TouchableOpacity>);
	}

	return (
		<View style={{
			flex: 1,
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			<ScrollView style={{
				flex: 1,
				backgroundColor: 'white',
				width: Common.getLengthByIPhone7(0)
			}}
			contentContainerStyle={{
				alignItems: 'center',
				justifyContent: 'flex-start',
			}}>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(24),
					fontFamily: 'Gilroy-ExtraBold',
					fontSize: Common.getLengthByIPhone7(27),
					color: colors.GRAY1_COLOR,
				}}
				allowFontScaling={false}>
					Укажите свои реквизиты для выплат
				</Text>
				<View style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
					borderRadius: Common.getLengthByIPhone7(20),
					padding: Common.getLengthByIPhone7(25),
					backgroundColor: 'white',
					shadowColor: "black",
					shadowOffset: {
						width: 0,
						height: 0,
					},
					shadowOpacity: 0.05,
					shadowRadius: 32.00,
					elevation: 1,
					marginTop: Common.getLengthByIPhone7(30),
				}}>
					<Text style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(80),
						fontFamily: 'Gilroy-Bold',
						fontSize: Common.getLengthByIPhone7(18),
						color: colors.GRAY3_COLOR,
					}}
					allowFontScaling={false}>
						Реквезиты
					</Text>
					<TextView
						style={{

						}}
						placeholder={'Наименование ООО или ИП'}
						max={150}
						onChange={text => {

						}}
					/>
					<TextView
						style={{

						}}
						placeholder={'Наименование банка'}
						max={150}
						onChange={text => {

						}}
					/>
					<TextView
						style={{

						}}
						placeholder={'Расчетный счет'}
						max={150}
						keyboardType={'number-pad'}
						onChange={text => {

						}}
					/>
					<TextView
						style={{

						}}
						placeholder={'БИК'}
						max={150}
						keyboardType={'number-pad'}
						onChange={text => {

						}}
					/>
					<TextView
						style={{

						}}
						placeholder={'ИНН'}
						max={150}
						keyboardType={'number-pad'}
						onChange={text => {

						}}
					/>
				</View>
				<BlueButton
					style={{
						marginTop: Common.getLengthByIPhone7(50),
						marginBottom: Common.getLengthByIPhone7(50) + 56,
					}}
					title={'Готово'}
					onPress={() => {
						navigation.navigate('RekvizitsSaved', {action: route?.params?.action});
						if (route?.params?.action) {
							route?.params?.action();
						}
					}}
				/>
			</ScrollView>
		</View>
	);
};

const mstp = (state: RootState) => ({
	dictionaries: state.user.dictionaries,
});

const mdtp = (dispatch: Dispatch) => ({
	setSkipName: (payload) => dispatch.buttons.setSkipName(payload),
	saveSelf: (payload) => dispatch.user.saveSelf(payload),
});

export default connect(mstp, mdtp)(RekvizitsScreen);
