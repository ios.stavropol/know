import React, { useCallback, useEffect, useRef } from 'react';
import { StyleSheet, StatusBar, View, Image, TouchableOpacity, Text, AppState } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';

const Splash2Screen = ({getSelf, getAuthorPlans, getDiskSpacePlans, getUserPlans, getAuthor, getDictionaries}) => {

  const navigation = useNavigation();

  useEffect(() => {
    StorageHelper.getData('token')
    .then(token => {
      if (token?.length) {
        API.setToken(token);
        getSelf()
        .then(profile => {
          navigation.navigate('LoginIn');
          getDictionaries();
          getAuthorPlans();
          getDiskSpacePlans();
          getUserPlans();
        })
        .catch(err => {
          navigation.navigate('Enter');
        });
      } else {
        navigation.navigate('Enter');
      }
    })
    .catch(err => {
      navigation.navigate('Enter');
    });
    SplashScreen.hide();
  }, []);

  return (
    <View style={{
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
    }}>
      <Image source={require('./../../assets/splash.png')}
        style={{
          width: Common.getLengthByIPhone7(0),
          flex: 1,
          resizeMode: 'cover',
        }}
      />
    </View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	getSelf: () => dispatch.user.getSelf(),
  getAuthor: () => dispatch.user.getAuthor(),
  getDictionaries: () => dispatch.user.getDictionaries(),
  getDiskSpacePlans: () => dispatch.user.getDiskSpacePlans(),
  getUserPlans: () => dispatch.user.getUserPlans(),
  getAuthorPlans: () => dispatch.user.getAuthorPlans(),
});

export default connect(mstp, mdtp)(Splash2Screen);
