import React, { useCallback, useEffect, useRef } from 'react';
import { Platform, View, ScrollView, FlatList, TouchableOpacity, Text, Image, StatusBar, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import BlueButton from '../../components/BlueButton';

const RekvizitsSavedScreen = ({route}) => {

	const navigation = useNavigation();

	useEffect(() => {
		
	}, []);

	return (
		<View style={{
			flex: 1,
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'flex-end',
		}}>
			<Image
				source={require('./../../assets/ic-check-circle.png')}
				style={{
					width: Common.getLengthByIPhone7(203),
					height: Common.getLengthByIPhone7(203),
					resizeMode: 'contain',
				}}
			/>
			<Text style={{
				fontFamily: 'Gilroy-Bold',
				fontSize: Common.getLengthByIPhone7(36),
				color: 'black',
				marginTop: Common.getLengthByIPhone7(95),
			}}
			allowFontScaling={false}>
				Готово!
			</Text>
			<Text style={{
				fontFamily: 'Gilroy-Medium',
				fontSize: Common.getLengthByIPhone7(18),
				color: colors.GRAY3_COLOR,
				marginTop: Common.getLengthByIPhone7(10),
			}}
			allowFontScaling={false}>
				Ваши реквизиты сохранены
			</Text>
			<BlueButton
				style={{
					marginTop: Common.getLengthByIPhone7(30),
					marginBottom: Common.getLengthByIPhone7(50) + 56,
				}}
				title={'Создать курс'}
				onPress={() => {
					navigation.navigate('Create');
				}}
			/>
		</View>
	);
};

const mstp = (state: RootState) => ({
	dictionaries: state.user.dictionaries,
});

const mdtp = (dispatch: Dispatch) => ({
	setSkipName: (payload) => dispatch.buttons.setSkipName(payload),
	saveSelf: (payload) => dispatch.user.saveSelf(payload),
});

export default connect(mstp, mdtp)(RekvizitsSavedScreen);
