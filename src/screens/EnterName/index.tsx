import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Platform, View, TextInput, TouchableOpacity, Text, StatusBar } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import BlueButton from '../../components/BlueButton';
import { VERSION } from '../../constants';

const EnterNameScreen = ({setSkipName}) => {

	const navigation = useNavigation();

	const input = useRef(null);
	const [name, setName] = React.useState('');
	const [showError, setShowError] = React.useState(0);

	useEffect(() => {
		setSkipName(next);
		if (Platform.OS === 'ios') {
			StatusBar.setBarStyle('dark-content', true);
		} else {
			StatusBar.setBarStyle('dark-content', true);
			StatusBar.setBackgroundColor(colors.WHITE_COLOR, true);
		}
	}, []);

	const next = () => {
		navigation.navigate('SelectCategories', {name: name});
	}

	return (
		<View style={{
			flex: 1,
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			<View>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(34),
					fontFamily: 'Gilroy-Light',
					fontSize: Common.getLengthByIPhone7(20),
					lineHeight: Common.getLengthByIPhone7(30),
					color: colors.GRAY3_COLOR,
				}}
				allowFontScaling={false}>
					Немного о себе
				</Text>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(34),
					fontFamily: 'Gilroy-Bold',
					fontSize: Common.getLengthByIPhone7(30),
					lineHeight: Common.getLengthByIPhone7(36),
					color: '#333333',
					marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					Введите ваше имя
				</Text>
				<TextInput
					style={[{
						marginTop: Common.getLengthByIPhone7(134),
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
						minHeight: Common.getLengthByIPhone7(65),
						borderRadius: Common.getLengthByIPhone7(12),
						backgroundColor: '#f4f5f6',
						textAlign: 'center',
						fontFamily: 'Gilroy-Regular',
						fontSize: Common.getLengthByIPhone7(30),
						paddingLeft: Common.getLengthByIPhone7(10),
						paddingRight: Common.getLengthByIPhone7(10),
						color: showError === 0 ? colors.BLACK_COLOR : (showError === 1 ? colors.GREEN1_COLOR : colors.RED_COLOR),
					}]}
					multiline={false}
					numberOfLines={1}
					autoFocus
					ref={input}
					placeholderTextColor={colors.GRAY4_COLOR}
					placeholder={'Имя'}
					contextMenuHidden={false}
					autoCorrect={false}
					autoCompleteType={'off'}
					returnKeyType={'done'}
					secureTextEntry={false}
					keyboardType={'email-address'}
					allowFontScaling={false}
					underlineColorAndroid={'transparent'}
					onChangeText={text => {
						setName(text);
						setShowError(0);
					}}
					onBlur={() => {
						setShowError(1);
					}}
					value={name}
				/>
			</View>
			{showError === 1 ? (<BlueButton
				style={{
					marginBottom: Common.getLengthByIPhone7(50),
				}}
				title={'Продолжить'}
				onPress={() => {
					if (name?.length) {
						next();
					}
				}}
			/>) : null}
		</View>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	setSkipName: (payload) => dispatch.buttons.setSkipName(payload),
});

export default connect(mstp, mdtp)(EnterNameScreen);
