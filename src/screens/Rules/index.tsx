import React, { useCallback, useEffect, useRef } from 'react';
import { Image, Platform, View, TextInput, TouchableOpacity, Text, StatusBar } from 'react-native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';

const RulesScreen = ({}) => {

	const navigation = useNavigation();

	const input = useRef(null);
	const [email, setEmail] = React.useState('');
	const [showError, setShowError] = React.useState(0);

	useEffect(() => {
		if (Platform.OS === 'ios') {
			StatusBar.setBarStyle('dark-content', true);
		} else {
			StatusBar.setBarStyle('dark-content', true);
			StatusBar.setBackgroundColor(colors.WHITE_COLOR, true);
		}
	}, []);

	const renderView = (title, style, action) => {
		return (<TouchableOpacity style={[{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
			height: Common.getLengthByIPhone7(70),
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
			borderBottomColor: colors.GRAY4_COLOR,
			borderBottomWidth: 1,
		}, style]}
		onPress={() => {
			if (action) {
				action();
			}
		}}>
			<Text style={{
				fontFamily: 'Gilroy-Medium',
				fontSize: Common.getLengthByIPhone7(17),
				color: colors.GRAY1_COLOR,
			}}
			allowFontScaling={false}>
				{title}
			</Text>
			<Image
				source={require('./../../assets/ic-arrow-right.png')}
				style={{
					width: Common.getLengthByIPhone7(15),
					height: Common.getLengthByIPhone7(15),
					resizeMode: 'contain',
				}}
			/>
		</TouchableOpacity>);
	}

	return (
		<View style={{
			flex: 1,
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			<View>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(34),
					fontFamily: 'Gilroy-Bold',
					fontSize: Common.getLengthByIPhone7(30),
					lineHeight: Common.getLengthByIPhone7(36),
					color: '#333333',
					marginBottom: Common.getLengthByIPhone7(30),
				}}
				allowFontScaling={false}>
					Правила и соглашения
				</Text>
				{renderView('Политика конфиденциальности', {

				}, () => {
					navigation.navigate('RuleView', {
						name: 'Политика конфиденциальности',
						url: 'https://knowapp.ru/privacy-policy',
					});
				})}
				{renderView('Пользовательское соглашение', {

				}, () => {
					navigation.navigate('RuleView', {
						name: 'Пользовательское соглашение',
						url: 'https://knowapp.ru/user-agreement',
					});
				})}
			</View>
		</View>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(RulesScreen);
