import React, { useCallback, useEffect, useRef } from 'react';
import { ScrollView, Platform, View, KeyboardAvoidingView, Alert, Text, StatusBar } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import PhotoView from '../../components/AuthorAnketa/PhotoView';
import TextView from '../../components/AuthorAnketa/TextView';
import SocialView from '../../components/AuthorAnketa/SocialView';
import AboutView from '../../components/AuthorAnketa/AboutView';
import PickerView from '../../components/AuthorAnketa/PickerView';
import BlueButton from '../../components/BlueButton';
import { APP_NAME } from '../../constants';

const AuthorAnketaScreen = ({uploadPhoto, route, createAuthor, saveAuthor}) => {

	const navigation = useNavigation();

	const [name, setName] = React.useState('');
	const [about, setAbout] = React.useState('');
	const [photo, setPhoto] = React.useState(null);
	const [category, setCategory] = React.useState(0);
	const [socials, setSocials] = React.useState([]);

	useEffect(() => {
		
	}, []);

	const next = () => {
		if (name.length === 0) {
			Alert.alert(APP_NAME, 'Укажите ФИО!');
			return;
		}
		if (photo === null) {
			Alert.alert(APP_NAME, 'Загрузите фото');
			return;
		}
		if (about.length === 0) {
			Alert.alert(APP_NAME, 'Расскажите о себе!');
			return;
		}
		if (category === 0) {
			Alert.alert(APP_NAME, 'Укажите вашу сферу!');
			return;
		}
		uploadPhoto(photo)
		.then(obj => {
			saveAuthor({
				image_id: obj.id,
				social_networks: socials,
				about_courses: category.id,
				about_me: about,
				full_name: name,
			})
			.then(profile => {
				route?.params?.action();
				navigation.goBack();
			})
			.catch(err => {

			});
		})
		.catch(err => {

		});
	}

	return (
		<View style={{
			flex: 1,
			backgroundColor: colors.BACKGROUND_COLOR,
			alignItems: 'center',
			justifyContent: 'flex-start',
		}}>
			<KeyboardAvoidingView
				behavior={Platform.OS === "ios" ? "padding" : "height"}
				style={{
					alignItems: 'center',
					justifyContent: 'flex-start',
					flex: 1,
				}}
			>
				<ScrollView style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					marginBottom: 86,
				}}
				contentContainerStyle={{
					alignItems: 'center',
				}}>
					<Text style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
						fontFamily: 'Gilroy-ExtraBold',
						fontSize: Common.getLengthByIPhone7(36),
						color: colors.GRAY1_COLOR,
					}}>
						Анкета автора
					</Text>
					<PhotoView
						style={{
							marginTop: Common.getLengthByIPhone7(37),
						}}
						onChange={obj => {
							setPhoto(obj);
						}}
					/>
					<TextView
						style={{
							marginTop: Common.getLengthByIPhone7(20),
						}}
						title={'Давай знакомиться'}
						placeholder={'ФИО'}
						max={50}
						onChange={text => {
							setName(text);
						}}
					/>
					<PickerView
						style={{
							marginTop: Common.getLengthByIPhone7(20),
						}}
						title={'В какой сфере ваш курс'}
						placeholder={'Выберете одну из сфер'}
						navigation={navigation}
						onChange={obj => {
							setCategory(obj);
						}}
					/>
					<SocialView
						style={{
							marginTop: Common.getLengthByIPhone7(20),
						}}
						onChange={text => {
							setSocials(text);
						}}
					/>
					<AboutView
						style={{
							marginTop: Common.getLengthByIPhone7(20),
						}}
						onChange={text => {
							setAbout(text);
						}}
					/>
					<BlueButton
						style={{
							marginTop: Common.getLengthByIPhone7(30),
							marginBottom: Common.getLengthByIPhone7(30),
						}}
						title={'Сохранить'}
						onPress={() => {
							next();
						}}
					/>
				</ScrollView>
			</KeyboardAvoidingView>
		</View>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	uploadPhoto: (payload) => dispatch.user.uploadPhoto(payload),
	createAuthor: (payload) => dispatch.user.createAuthor(payload),
	saveAuthor: (payload) => dispatch.user.saveAuthor(payload),
});

export default connect(mstp, mdtp)(AuthorAnketaScreen);
