import React, { useCallback, useEffect, useRef } from 'react';
import { Platform, View, ScrollView, TouchableOpacity, Text, Image, StatusBar, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import BlueButton from '../../components/BlueButton';
import { APP_NAME } from '../../constants';
import AuthorView from '../../components/ActivateAuthor/AuthorView';
import DiskView from '../../components/ActivateAuthor/DiskView';

const ActivateAuthorScreen = ({route, diskSpacePlans, userPlans, authorPlans, saveSelf}) => {

	const navigation = useNavigation();

	const [body, setBody] = React.useState([]);

	useEffect(() => {
		console.warn('diskSpacePlans: ', diskSpacePlans);
		console.warn('userPlans: ', userPlans);
		console.warn('authorPlans: ', authorPlans);
		let array = [];
		for (let i = 0; i < authorPlans.length; i++) {
			array.push(<AuthorView
				style={{
					marginTop: Common.getLengthByIPhone7(42),
					marginBottom: Common.getLengthByIPhone7(25),
				}}
				data={authorPlans[i]}
				onChange={obj => {

				}}
			/>);
		}
		setBody(array);
	}, []);

	return (
		<View style={{
			flex: 1,
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			<ScrollView style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
			}}
			contentContainerStyle={{
				alignItems: 'center',
			}}>
				<View style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					alignItems: 'center',
				}}>
					<Text style={{
						width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(24),
						fontFamily: 'Gilroy-ExtraBold',
						fontSize: Common.getLengthByIPhone7(27),
						color: colors.GRAY1_COLOR,
					}}
					allowFontScaling={false}>
						{`Активировать\nаккаунт автора`}
					</Text>
					{body}
					<DiskView
						style={{
							marginBottom: Common.getLengthByIPhone7(15),
						}}
						data={diskSpacePlans}
					/>
				</View>
				<BlueButton
					style={{
						marginBottom: Common.getLengthByIPhone7(50),
					}}
					title={'Готово'}
					onPress={() => {
						navigation.navigate('Rekvizits', {action: route?.params?.action});
					}}
				/>
			</ScrollView>
		</View>
	);
};

const mstp = (state: RootState) => ({
	dictionaries: state.user.dictionaries,
	diskSpacePlans: state.user.diskSpacePlans,
	authorPlans: state.user.authorPlans,
	userPlans: state.user.userPlans,
});

const mdtp = (dispatch: Dispatch) => ({
	setSkipName: (payload) => dispatch.buttons.setSkipName(payload),
	saveSelf: (payload) => dispatch.user.saveSelf(payload),
});

export default connect(mstp, mdtp)(ActivateAuthorScreen);
