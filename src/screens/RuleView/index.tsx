import React, { useCallback, useEffect, useRef } from 'react';
import { Image, Platform, View, TextInput, TouchableOpacity, Text, StatusBar } from 'react-native';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { WebView } from 'react-native-webview';

const RuleViewScreen = ({route}) => {

	const navigation = useNavigation();
	const [shadow, setShadow] = React.useState(false);

	return (
		<View style={{
			flex: 1,
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			<View>
				<View style={{
					width: Common.getLengthByIPhone7(0),
					zIndex: 10,
				}}>
					<View style={{
						backgroundColor: 'white',
						alignItems: 'center',
						width: Common.getLengthByIPhone7(0),
						paddingBottom: Common.getLengthByIPhone7(20),
						zIndex: 10,
					}}>
						<Text style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
							fontFamily: 'Gilroy-Bold',
							fontSize: Common.getLengthByIPhone7(30),
							lineHeight: Common.getLengthByIPhone7(36),
							color: '#333333',
						}}
						allowFontScaling={false}>
							{route?.params?.name}
						</Text>
					</View>
					<View style={{
						backgroundColor: 'white',
						height: 10,
						width: Common.getLengthByIPhone7(0),
						position: 'absolute',
						bottom: 0,
						left: 0,
						shadowColor: "black",
						shadowOffset: {
							width: 0,
							height: 2,
						},
						shadowOpacity: 0.5,
						shadowRadius: 15.00,
						elevation: 5,
						opacity: shadow ? 1 : 0,
					}}/>
				</View>
				<View style={{
					width: Common.getLengthByIPhone7(0),
					alignItems: 'center',
					flex: 1,
				}}>
					<WebView
						source={{ uri: route?.params?.url}}
						style={{
							width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
							flex: 1,
							alignItems: 'center',
						}}
						onScroll={event => {
							if (event.nativeEvent.contentOffset.y > 0) {
								setShadow(true);
							} else {
								setShadow(false);
							}
						}}
					/>
				</View>
			</View>
		</View>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(RuleViewScreen);
