import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Platform, View, TouchableOpacity, Animated, Text, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import BlueButton from '../../components/BlueButton';
import {
	CodeField,
	Cursor,
	useBlurOnFulfill,
	useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { APP_NAME } from '../../constants';

const {Value, Text: AnimatedText} = Animated;

const CELL_HEIGHT = Common.getLengthByIPhone7(65);
const CELL_WIDTH = Common.getLengthByIPhone7(50);
const CELL_BORDER_RADIUS = Common.getLengthByIPhone7(12);
const NOT_EMPTY_CELL_BG_COLOR = '#f4f5f6';
const ACTIVE_CELL_BG_COLOR = '#f4f5f6';
const CELL_COUNT = 4;

const animationsColor = [...new Array(CELL_COUNT)].map(() => new Value(0));
const animationsScale = [...new Array(CELL_COUNT)].map(() => new Value(1));
const animateCell = ({hasValue, index, isFocused}) => {
  Animated.parallel([
    Animated.timing(animationsColor[index], {
      useNativeDriver: false,
      toValue: isFocused ? 1 : 0,
      duration: 250,
    }),
    Animated.spring(animationsScale[index], {
      useNativeDriver: false,
      toValue: hasValue ? 0 : 1,
      duration: hasValue ? 300 : 250,
    }),
  ]).start();
};

const EnterCodeScreen = ({route, getCode, getAuthorPlans, getDiskSpacePlans, getUserPlans, login, getSelf, getDictionaries}) => {

	const navigation = useNavigation();

	const input = useRef(null);
	const [showError, setShowError] = React.useState(0);

	const [value, setValue] = React.useState('');
	const [error, setError] = React.useState(false);

	const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
	const [props, getCellOnLayoutHandler] = useClearByFocusCell({
		value,
		setValue,
	});

	useEffect(() => {
		setValue(value.replace(/[^\d]/,''));
		setError(false);
		if (value?.length === 4) {
			login({
				email: route?.params?.email,
				code: value,
			})
			.then(() => {
				getSelf()
				.then(profile => {
					if (profile?.email_verified_at?.length) {
						Alert.alert(APP_NAME, 'Переход на этап 2');
					} else {
						navigation.navigate('SelfSave');
					}
				})
				.catch(err => {
					Alert.alert(APP_NAME, err);
				});
				getDictionaries();
				getAuthorPlans();
				getDiskSpacePlans();
				getUserPlans();
			})
			.catch(err => {
				// Alert.alert(APP_NAME, err);
				setError(true);
			});
		}
	}, [value]);

	useEffect(() => {
		setTimeout(() => {
			if (ref) {
				ref.current?.focus();
			}
		}, 500);
	}, []);

	const renderCell = ({index, symbol, isFocused}) => {
		const hasValue = Boolean(symbol);
		const animatedCellStyle = {
			backgroundColor: ACTIVE_CELL_BG_COLOR,
			borderRadius: CELL_BORDER_RADIUS,
		};
	
		setTimeout(() => {
		  animateCell({hasValue, index, isFocused});
		}, 0);
	
		return (
		<View style={{
			height: CELL_HEIGHT,
			width: CELL_WIDTH,
			borderRadius: CELL_BORDER_RADIUS,
			overflow: 'hidden',
			marginHorizontal: Common.getLengthByIPhone7(10),
			alignItems: 'center',
			justifyContent: 'center',
			backgroundColor: ACTIVE_CELL_BG_COLOR,
		}}>
			<AnimatedText
				key={index}
				style={[{
					fontFamily: 'Gilroy-Regular',
					fontSize: Common.getLengthByIPhone7(30),
					textAlign: 'center',
					color: error ? colors.RED_COLOR : colors.BLACK_COLOR,
				}, animatedCellStyle]}
				onLayout={getCellOnLayoutHandler(index)}>
				{symbol || (isFocused ? <Cursor /> : null)}
			</AnimatedText>
		</View>
		);
	};

  return (
    <View style={{
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
		justifyContent: 'space-between',
    }}>
		<View>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(34),
				fontFamily: 'Gilroy-Light',
				fontSize: Common.getLengthByIPhone7(20),
				lineHeight: Common.getLengthByIPhone7(30),
				color: colors.GRAY3_COLOR,
			}}
			allowFontScaling={false}>
				Верификация
			</Text>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(34),
				fontFamily: 'Gilroy-Bold',
				fontSize: Common.getLengthByIPhone7(30),
				lineHeight: Common.getLengthByIPhone7(36),
				color: '#333333',
				marginTop: Common.getLengthByIPhone7(10),
			}}
			allowFontScaling={false}>
				Мы отправили вам код подтверждения
			</Text>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(34),
				fontFamily: 'Gilroy-Light',
				fontSize: Common.getLengthByIPhone7(20),
				lineHeight: Common.getLengthByIPhone7(30),
				color: colors.GRAY3_COLOR,
			}}
			allowFontScaling={false}>
				На почту: <Text style={{
					fontFamily: 'Gilroy-Medium',
					fontSize: Common.getLengthByIPhone7(20),
					lineHeight: Common.getLengthByIPhone7(30),
					color: colors.BLUE_COLOR,
				}}
				allowFontScaling={false}>
					{route?.params?.email}
				</Text>
			</Text>
			<CodeField
				ref={ref}
				{...props}
				value={value}
				onChangeText={setValue}
				cellCount={CELL_COUNT}
				rootStyle={{
					height: CELL_HEIGHT,
					marginTop: Common.getLengthByIPhone7(91),
					paddingHorizontal: Common.getLengthByIPhone7(24),
					justifyContent: 'center',
				}}
				keyboardType="number-pad"
				textContentType="oneTimeCode"
				renderCell={renderCell}
			/>
			<TouchableOpacity style={{
				marginTop: Common.getLengthByIPhone7(30),
			}}
			onPress={() => {
				setValue('');
				ref.current?.focus();
				getCode(route?.params?.email);
			}}>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(30),
					fontFamily: 'Gilroy-Medium',
					fontSize: Common.getLengthByIPhone7(15),
					lineHeight: Common.getLengthByIPhone7(18),
					color: colors.BLUE_COLOR,
					textAlign: 'center',
				}}
				allowFontScaling={false}>
					Отправить код снова
				</Text>
			</TouchableOpacity>
		</View>
		{showError === 1 ? (<BlueButton
			style={{
				marginBottom: Common.getLengthByIPhone7(50),
			}}
			title={'Продолжить'}
			onPress={() => {
				
			}}
		/>) : null}
    </View>
  );
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	getCode: (payload) => dispatch.user.getCode(payload),
	login: (payload) => dispatch.user.login(payload),
	getSelf: () => dispatch.user.getSelf(),
	getDictionaries: () => dispatch.user.getDictionaries(),
	getDiskSpacePlans: () => dispatch.user.getDiskSpacePlans(),
	getUserPlans: () => dispatch.user.getUserPlans(),
	getAuthorPlans: () => dispatch.user.getAuthorPlans(),
});

export default connect(mstp, mdtp)(EnterCodeScreen);
