import React, { useCallback, useEffect, useRef } from 'react';
import { Platform, View, FlatList, TouchableOpacity, Text, Image, StatusBar, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import BlueButton from '../../components/BlueButton';
import { APP_NAME } from '../../constants';

const SelectCategoriesScreen = ({route, saveSelf, dictionaries, setSkipName}) => {

	const navigation = useNavigation();

	const [selected, setSelected] = React.useState([]);

	useEffect(() => {
		setSkipName(skipFun);
		if (Platform.OS === 'ios') {
			StatusBar.setBarStyle('dark-content', true);
		} else {
			StatusBar.setBarStyle('dark-content', true);
			StatusBar.setBackgroundColor(colors.WHITE_COLOR, true);
		}
	}, []);

	const skipFun = () => {
		saveSelf({
			name: '',
			category_ids: [],
		})
		.then(() => {
			navigation.navigate('LoginIn');
		})
		.catch(err => {
			Alert.alert(APP_NAME, err);
		});
	}

	const next = () => {
		saveSelf({
			name: route?.params?.name,
			category_ids: selected,
		})
		.then(() => {
			navigation.navigate('LoginIn');
		})
		.catch(err => {
			Alert.alert(APP_NAME, err);
		});
	}

	const renderRow = (item: object, index: number) => {
		return (<TouchableOpacity style={{
			width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(60),
			paddingTop: Common.getLengthByIPhone7(10),
			paddingBottom: Common.getLengthByIPhone7(10),
			flexDirection: 'row',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}
		onPress={() => {
			let array = JSON.parse(JSON.stringify(selected));
			if (array.includes(item?.id)) {
				for (let i = 0; i < array.length; i++) {
					if (array[i] === item?.id) {
						array.splice(i, 1);
						break;
					}
				}
			} else {
				if (array.length < 3) {
					array.push(item?.id);
				} else {
					
				}
			}
			setSelected(array);
		}}>
			<Text style={{
				width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(100),
				fontFamily: selected.includes(item?.id) ? 'Gilroy-SemiBold' : 'Gilroy-Regular',
				fontSize: Common.getLengthByIPhone7(20),
				lineHeight: Common.getLengthByIPhone7(30),
				color: colors.GRAY2_COLOR,
			}}
			allowFontScaling={false}>
				{item?.name}
			</Text>
			{selected.includes(item?.id) ? (<View style={{
				width: Common.getLengthByIPhone7(20),
				height: Common.getLengthByIPhone7(20),
				borderRadius: Common.getLengthByIPhone7(10),
				alignItems: 'center',
				justifyContent: 'center',
				backgroundColor: colors.BLUE_COLOR,
			}}>
				<Image
					source={require('./../../assets/ic-check.png')}
					style={{
						width: Common.getLengthByIPhone7(12),
						height: Common.getLengthByIPhone7(12),
						resizeMode: 'contain',
					}}
				/>
			</View>) : (<View style={{
				width: Common.getLengthByIPhone7(20),
				height: Common.getLengthByIPhone7(20),
				borderRadius: Common.getLengthByIPhone7(10),
				alignItems: 'center',
				justifyContent: 'center',
				borderColor: colors.BLUE_COLOR,
				borderWidth: 2,
				opacity: 0.5,
			}}/>)}
		</TouchableOpacity>);
	}

	return (
		<View style={{
			flex: 1,
			backgroundColor: 'white',
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			<View style={{
				width: Common.getLengthByIPhone7(0),
				flex: 1,
				alignItems: 'center',
			}}>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(34),
					fontFamily: 'Gilroy-Light',
					fontSize: Common.getLengthByIPhone7(20),
					lineHeight: Common.getLengthByIPhone7(30),
					color: colors.GRAY3_COLOR,
				}}
				allowFontScaling={false}>
					Немного о себе
				</Text>
				<Text style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(24),
					fontFamily: 'Gilroy-Bold',
					fontSize: Common.getLengthByIPhone7(24),
					lineHeight: Common.getLengthByIPhone7(32),
					color: '#333333',
					marginTop: Common.getLengthByIPhone7(10),
				}}
				allowFontScaling={false}>
					Выберите три приоритетных сферы для себя
				</Text>
				<FlatList
					style={{
						flex: 1,
						backgroundColor: 'transparent',
						width: Common.getLengthByIPhone7(0),
						marginTop: Common.getLengthByIPhone7(10),
					}}
					contentContainerStyle={{
						alignItems: 'center',
						justifyContent: 'flex-start',
					}}
					removeClippedSubviews={false}
					scrollEventThrottle={16}
					data={dictionaries}
					extraData={dictionaries}
					keyExtractor={(item, index) => index.toString()}
					renderItem={({item, index}) => renderRow(item, index)}
				/>
			</View>
			<BlueButton
				style={{
					marginBottom: Common.getLengthByIPhone7(50),
				}}
				title={'Готово'}
				onPress={() => {
					next();
				}}
			/>
		</View>
	);
};

const mstp = (state: RootState) => ({
	dictionaries: state.user.dictionaries,
});

const mdtp = (dispatch: Dispatch) => ({
	setSkipName: (payload) => dispatch.buttons.setSkipName(payload),
	saveSelf: (payload) => dispatch.user.saveSelf(payload),
});

export default connect(mstp, mdtp)(SelectCategoriesScreen);
