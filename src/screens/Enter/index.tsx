import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Platform, View, Image, TouchableOpacity, Text, Alert } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import { theme } from './../../../theme';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import { useFocusEffect } from '@react-navigation/native';
import BlueButton from '../../components/BlueButton';
import { VERSION, APP_NAME } from '../../constants';
import { AppleButton, appleAuth } from '@invertase/react-native-apple-authentication';
import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';

const EnterScreen = ({mobileLogin, getSelf, getDictionaries}) => {

	const navigation = useNavigation();

	useEffect(() => {
		if (Platform.OS === 'android') {
			GoogleSignin.configure({
				offlineAccess: true,
				webClientId: '354737388252-tuqh9tlhaueti5jlhho3pnrvvvbgjhe3.apps.googleusercontent.com',
			});
			auth().signOut().then(() => console.warn('User signed out!'));
			let subscriber = auth().onAuthStateChanged(onAuthStateChanged);
			return subscriber;
		}
	}, []);

	const onAuthStateChanged = user => {
		console.warn('user: ', user);
		user?.getIdToken()
		.then(idToken => {
			mobileLogin(idToken)
			.then(() => {
				getSelf()
				.then(profile => {
					if (profile?.email_verified_at?.length) {
						Alert.alert(APP_NAME, 'Переход на этап 2');
					} else {
						navigation.navigate('SelfSave');
					}
				})
				.catch(err => {
					Alert.alert(APP_NAME, err);
				});
				getDictionaries();
			})
			.catch(err => {
				Alert.alert(APP_NAME, err);
			});
		})
		.catch(err => {
			console.warn('getIdToken err: ', err);
		});
	}

	async function onGoogleButtonPress() {
		const userInfo = await GoogleSignin.signIn();
	  
		console.warn('userInfo: ', userInfo);
		const googleCredential = auth.GoogleAuthProvider.credential(userInfo.idToken);
	  	return auth().signInWithCredential(googleCredential);
	}

	async function onAppleButtonPress() {
		// performs login request
		const appleAuthRequestResponse = await appleAuth.performRequest({
		  requestedOperation: appleAuth.Operation.LOGIN,
		  requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
		});
	  
		// get current authentication state for user
		// /!\ This method must be tested on a real device. On the iOS simulator it always throws an error.
		const credentialState = await appleAuth.getCredentialStateForUser(appleAuthRequestResponse.user);
	  
		console.warn('credentialState: ', credentialState);
		console.warn('appleAuth: ', appleAuth);
		console.warn('appleAuthRequestResponse: ', appleAuthRequestResponse);
		const { identityToken, nonce } = appleAuthRequestResponse;
		const appleCredential = auth.AppleAuthProvider.credential(identityToken, nonce);

		// Sign the user in with the credential
		// return 
		return auth().signInWithCredential(appleCredential);
		// use credentialState response to ensure the user is authenticated
		if (credentialState === appleAuth.State.AUTHORIZED) {
		  // user is authenticated
		}
	}

	return (
		<View style={{
			flex: 1,
			backgroundColor: 'white',
			alignItems: 'center',
		}}>
			<ImageBackground source={require('./../../assets/ic-start.png')}
				style={{
					width: Common.getLengthByIPhone7(0),
					flex: 1,
					resizeMode: 'cover',
					alignItems: 'center',
					justifyContent: 'flex-end',
				}}
			>
				<BlueButton
					style={{
						marginBottom: Common.getLengthByIPhone7(20),
					}}
					title={'Войти по E-mail'}
					onPress={() => {
						navigation.navigate('EnterEmail');
					}}
				/>
				<TouchableOpacity style={{
					width: Common.getLengthByIPhone7(0) - Common.getLengthByIPhone7(64),
					height: Common.getLengthByIPhone7(60),
					borderRadius: Common.getLengthByIPhone7(5),
					alignItems: 'center',
					justifyContent: 'center',
					backgroundColor: colors.WHITE_COLOR,
					marginBottom: Common.getLengthByIPhone7(20),
					flexDirection: 'row',
				}}
				onPress={() => {
					if (Platform.OS === 'ios') {
						onAppleButtonPress().then(() => console.warn('Apple sign-in complete!'));
					} else {
						onGoogleButtonPress()
						.then(() => {

						})
						.catch(err => {
							console.warn('onGoogleButtonPress err: ', err);
						});
					}
				}}>
					<Image
						source={Platform.OS === 'ios' ? require('./../../assets/ic-apple.png') : require('./../../assets/ic-google.png')}
						style={{
							width: Platform.OS === 'ios' ? Common.getLengthByIPhone7(17) : Common.getLengthByIPhone7(25),
							height: Platform.OS === 'ios' ? Common.getLengthByIPhone7(21) : Common.getLengthByIPhone7(25),
							resizeMode: 'contain',
						}}
					/>
					<Text style={{
						fontFamily: 'Gilroy-SemiBold',
						fontSize: Common.getLengthByIPhone7(18),
						lineHeight: Common.getLengthByIPhone7(21),
						color: colors.BLACK_COLOR,
						marginLeft: Platform.OS === 'ios' ? Common.getLengthByIPhone7(10) : 5,
					}}
					allowFontScaling={false}>
						Войти с помощью {Platform.OS === 'ios' ? 'Apple' : 'Google'}
					</Text>
				</TouchableOpacity>
				<Text style={{
					fontFamily: 'Gilroy-Medium',
					fontSize: Common.getLengthByIPhone7(12),
					color: colors.GRAY6_COLOR,
					marginBottom: Common.getLengthByIPhone7(50),
				}}
				allowFontScaling={false}>
					App version: {VERSION}
				</Text>
			</ImageBackground>
		</View>
	);
};

const mstp = (state: RootState) => ({
	
});

const mdtp = (dispatch: Dispatch) => ({
	mobileLogin: (payload) => dispatch.user.mobileLogin(payload),
	getSelf: () => dispatch.user.getSelf(),
	getDictionaries: () => dispatch.user.getDictionaries(),
});

export default connect(mstp, mdtp)(EnterScreen);
