import React, { useCallback, useEffect, useRef } from 'react';
import { ImageBackground, Platform, View, TextInput, TouchableOpacity, Text, StatusBar } from 'react-native';
import { API, StorageHelper } from './../../services';
import { RootState, Dispatch } from './../../store';
import { connect, useDispatch, useSelector } from 'react-redux';
import Common from './../../utilities/Common';
import {colors, typography, shadows} from './../../styles';
import { useNavigation } from '@react-navigation/native';
import NonAuthor from '../../components/Create/NonAuthor';
import NewAuthor from '../../components/Create/NewAuthor';
import ModerationView from '../../components/Create/ModerationView';
import PayedAuthor from '../../components/Create/PayedAuthor';

const CreateScreen = ({userProfile}) => {

	const navigation = useNavigation();
	const [body, setBody] = React.useState(null);

	useEffect(() => {
		if (userProfile.is_manager) {

		} else if (userProfile.is_author) {
			setBody(<NewAuthor
				navigation={navigation}
				action={() => {
					setBody(<PayedAuthor/>);
				}}
			/>);
		} else {
			setBody(<NonAuthor
				navigation={navigation}
				refresh={() => {
					setBody(<ModerationView/>);
				}}
			/>);
		}
	}, []);

	return (
		<View style={{
			flex: 1,
			backgroundColor: colors.BACKGROUND_COLOR,
			alignItems: 'center',
			justifyContent: 'space-between',
		}}>
			{body}
		</View>
	);
};

const mstp = (state: RootState) => ({
	userProfile: state.user.userProfile,
});

const mdtp = (dispatch: Dispatch) => ({
	
});

export default connect(mstp, mdtp)(CreateScreen);
