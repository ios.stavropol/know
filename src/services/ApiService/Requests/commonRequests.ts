import axios from 'axios';
import { BASE_URL } from './../../../constants';

class CommonAPI {
	getCode = (email: string) => {
		let params = JSON.stringify({
			email,
		});
		return axios.post(BASE_URL + '/api/mobile/send-code', params, {
			headers: {
			  
			}
		});
	};

	login = (email: string, code: string) => {
		let params = JSON.stringify({
			email,
			code,
		});
		return axios.post(BASE_URL + '/api/mobile/login', params, {
			headers: {
			  
			}
		});
	};

	mobileLogin = (token: string) => {
		let params = JSON.stringify({
			token,
		});
		return axios.post(BASE_URL + '/api/mobile/firebase-login', params, {
			headers: {
			  
			}
		});
	};

	getDictionaries = () => {
		return axios.get(BASE_URL + '/api/mobile/dictionaries/categories');
	};

	getDiskSpacePlans = () => {
		return axios.get(BASE_URL + '/api/mobile/dictionaries/disk-space-subscription-plans');
	};

	getUserPlans = () => {
		return axios.get(BASE_URL + '/api/mobile/dictionaries/user-subscription-plans');
	};

	getAuthorPlans = () => {
		return axios.get(BASE_URL + '/api/mobile/dictionaries/author-subscription-plans');
	};

	getSelf = () => {
		return axios.get(BASE_URL + '/api/mobile/self');
	};

	saveSelf = (payload: any) => {
		let params = JSON.stringify(payload);
		console.warn('payload: ', payload);
		return axios.put(BASE_URL + '/api/mobile/self', params, {
			headers: {
			  
			}
		});
	};

	uploadPhoto = (photo: string) => {
		var formData = new FormData();
		formData.append("file", {uri: photo, name: 'test.jpg', type: 'image/jpeg'});

		return axios.post(BASE_URL + '/api/attachments', formData, {
			headers: {
				'Content-Type': 'multipart/form-data',
			}
		});
	};

	getSelfAuthor = () => {
		return axios.get(BASE_URL + '/api/mobile/self/author');
	};

	saveAuthor = (payload: any) => {
		let params = JSON.stringify(payload);
		console.warn('payload: ', payload);
		return axios.post(BASE_URL + '/api/mobile/self/author', params, {
			headers: {
			  
			}
		});
	};

	createAuthor = (payload: any) => {
		let params = JSON.stringify(payload);
		console.warn('payload: ', payload);
		return axios.put(BASE_URL + '/api/mobile/self/author', params, {
			headers: {
			  
			}
		});
	};
}

const commonRequests = new CommonAPI();
export default commonRequests;
